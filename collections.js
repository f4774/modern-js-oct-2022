let images =  ['https://randomuser.me/api/portraits/med/men/2.jpg', 
                'https://randomuser.me/api/portraits/med/men/20.jpg',
                'https://randomuser.me/api/portraits/med/women/60.jpg',
                'https://randomuser.me/api/portraits/med/women/20.jpg' ];


// looping array

let arrplanets = ['Mercury', 'Mars', 'Earth'];
//arrplanets.forEach('as you loop thru each item... pass that item to the arrow function and then display the OP... ')
arrplanets.forEach((planetName) => { console.log('Name of the planet is ' + planetName)});


// Access one of the image tags and assign src img to it
// document.getElementByID
// document.getElementByTagName
// document.getQuerySelector
// document.getQuerySelectorAll

let imgElement = document.getElementById('image1');
imgElement.src = images[0]; // read from array and assign src attribute value

// Task: Assign src for each img take in the HTML
// 1. Find all image tags
let imgLst = document.getElementsByTagName('img') // it will return me a collection of HTML elements(img)
console.log('imgLst ::' + imgLst);
console.log('imgLst ::' + imgLst.length);
console.log('Type of imgLst is ' + typeof imgLst);
console.log('Type of arrplanets is ' + typeof arrplanets);


// 1.2 - Convert HTML collection into an array 
//let arrImgLst = Array.from(imgLst);  // Option 1: Array.form
let arrImgLst = [...imgLst]; // Option 2: Spread Opertor

// 2. Loop thru each img and read values from images array and then assign it to the src attribute
let index = 0;
arrImgLst.forEach((img) => {
    img.src = images[index];
    index++; 
});

// Task: Given an array of images, show all images dynamically on the screen
let imgCloud = [
                 'https://randomuser.me/api/portraits/med/men/12.jpg', 
                'https://randomuser.me/api/portraits/med/men/13.jpg',
                'https://randomuser.me/api/portraits/med/women/14.jpg',
                'https://randomuser.me/api/portraits/med/women/56.jpg',
                'https://randomuser.me/api/portraits/med/men/23.jpg', 
                'https://randomuser.me/api/portraits/med/men/22.jpg',
                'https://randomuser.me/api/portraits/med/women/6.jpg',
                'https://randomuser.me/api/portraits/med/women/59.jpg',
                'https://randomuser.me/api/portraits/med/men/55.jpg', 
                'https://randomuser.me/api/portraits/med/men/56.jpg',
                'https://randomuser.me/api/portraits/med/women/76.jpg',
                'https://randomuser.me/api/portraits/med/women/69.jpg'

];

// 1. Creating an element dynamically in JS
let imgTag = document.createElement('img');
imgTag.src = 'https://randomuser.me/api/portraits/men/75.jpg';

// 2. Append the newly created img tag into div
let divImagePanel = document.getElementById('imgPanel');
divImagePanel.appendChild(imgTag);

// 3. Loop thru each img link in the array
// 4. For each img link , create a new img tag dynamically and assign src atttribute
// 5. Finally, add them or append into the div panel

imgCloud.forEach((imgItem) => {
    let imgTag = document.createElement('img');
    imgTag.src = imgItem;
    divImagePanel.appendChild(imgTag);

});