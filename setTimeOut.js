// Use setTimeOut when u want to defer the execution of a particular piece of code

function createImageTag(source = 'https://randomuser.me/api/portraits/men/0.jpg', height = '200 px', width = '200 px', style = 'solid 4px red')
{
    let imgTag = document.createElement('img');
    imgTag.src = source;
    imgTag.style.height = height;
    imgTag.style.width = width;
    imgTag.style.border = style;
    document.body.appendChild(imgTag);
}
//createImageTag();

// param 1 = function ; param2 = delay in millisecounds
const delayedFunction = setTimeout(createImageTag,8000); // defing delay in milliseconds


// TODO: SetInterval()