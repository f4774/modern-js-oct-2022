
var pi = 3.14;
console.log(pi);
console.log(typeof pi);

var evennumber = 2;
console.log(typeof evennumber);

var largenumber = 2e5;
console.log(typeof largenumber);

// ES6 Std - let & const

const PlanetName = 'Earth';
console.log('Home Planent is ' + PlanetName);
//PlanetName = 'Mars'; // Error out cos consts cannnot be changed
//console.log('New Home Planent is ' + PlanetName);

const baseLoginURL = 'https://login.salesforec.com';
const bsaeTestURL = 'https://test.salesforce.com'; 
console.log('Base URL ' + baseLoginURL);

let x = 2
if(x == 2)
{
    let x = 5 // overridden x value
    console.log('Value of x is ' + x); // 5
}
console.log('Outside block - Value of x is ' + x); // 2 (let gives local scope to a variable)


var y = 20
if(y == 20)
{
    var y = 25 // overridden y value 
    console.log('Value of y is ' + y); // 35
}
console.log('Outside block - Value of y is ' + y); //25 (var gives variable a global scope)


z = 30 // by default it will have global scope and it will be part of windows object
console.log('Accessing via windows global object - Value of z is ' + window.z) // 30
console.log('Accessing directly - Value of z is ' + z) // 30 


// Arrays & Objects

// [] - array
// {} - object

let arrplanets = ['Mercury', 'Mars', 'Earth'];
let arrplanetsAndDistance = ['Mercury', 20000, 'Mars', 250000, 'Earth', 3.14, true];

console.log('Access array elements via index which starts from 0');
console.log('2rd planet from sun::' + arrplanets[1]);
console.log('3rd planet from sun::' + arrplanets[2]);
console.log('Length of arrplanets is ' + arrplanets.length);

// print all items from an array
for(let x=0; x < arrplanets.length; x++)
{
    //console.log('planet ' + arrplanets[x] + ' is located at ' +  x + ' poistion from the sun');

    console.log(`Planet ${arrplanets[x]} is located at ${x} poistion from the sun !!!!`)
}

// TODO:array for each 

let sandboxURLsToDeploy = Array(100);
console.log(`Total length allocated and kept ready is ${sandboxURLsToDeploy.length}`)


// Check if plannet earth is present in the array of planets
// Use Array.Find
let numbers = [1,2,3,4,5,6,7,8,9,10];
//let resultAllIteme = numbers.find( (item) => {console.log(`Item is ${item}`)});

let resultNumberGreaterThan5 = numbers.find( (item) => {                                                             
                                                if(item > 5)
                                                {
                                                    console.log('Value is greater than 5 !!!');
                                                    console.log(`Item is ${item}`);
                                                }
                                                });


// JS Functions

function SayHello(userName)
{
    console.log('Hi there ' + userName + '!!!');
}

SayHello('Saddam');

// Option 2: Nameless function
let SayHelloV2 = function(userName)
{
    console.log('Hi there ' + userName + '!!!');
};

SayHelloV2('Nishant');

// Option 3: Arrow function
let SayHelloV3 = (userName, role) => 
{
    console.log('Hi there ' + userName + '!!!');
    console.log(`User ${userName} is having role as ${role}`);
};

SayHelloV3('Thani', 'Developer');


function sum_tradional(a,b)
{
    return a + b;
}
console.log(sum_tradional(1,2));


let deep=(a,b) =>{
    return a+b; // When you have single statement inside fucntion curly braces 
}
console.log(deep(1,2));

let sum_Rajesh = (a,b) => {
        let input1 = a;
        let input2 = b;
        return input1 + input2;
}

console.log(sum_Rajesh(5,5));

let sum_Rajesh_V1 = (a,b) => 
{
    let input1 = a;
    let input2 = b;
     input1 + input2;

}



let sum = (a, b) => a+b;
console.log('Dhanshri...')
console.log(sum(2,2));



let objplanets = {
    'planet1': 'Mercury',
    'planet2': 'Mars',
    'planet3': 'Earth'
};