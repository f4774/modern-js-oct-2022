function greeting(name = 'there!!!')
{
    return 'Hi ' + name;
}

console.log('Output is ' + greeting('Mohan'));

console.log('Output is ' + greeting());

// Create an image tag dynamically and adds it to the page
function createImageTag(source = 'https://randomuser.me/api/portraits/men/0.jpg', height = '200 px', width = '200 px', style = 'solid 4px red')
{
    let imgTag = document.createElement('img');
    imgTag.src = source;
    imgTag.style.height = height;
    imgTag.style.width = width;
    imgTag.style.border = style;
    document.body.appendChild(imgTag);
}

createImageTag('https://randomuser.me/api/portraits/men/22.jpg', 
'300px','200px', 'solid 4px blue');

createImageTag('https://randomuser.me/api/portraits/women/22.jpg');
createImageTag();

function sum(a = 1, b = a, c = a + b)
{
    return a + b + c;
}

let output = sum(1);
console.log(output);

function CalculatePrice(price, discount = 0.1 * price)
{
    return price * discount;
}
let PriceAfterDiscount = CalculatePrice(100);
console.log('Product Price after discount is ' + PriceAfterDiscount);
